# minapp

Minimal smartdom application.

[Source](https://github.com/aestheticbookshelf/minapp)

[Online](https://abminapp.herokuapp.com/)

## Installation

```
git clone https://github.com/aestheticbookshelf/minapp
cd minapp
npm install
```

## Development server

### Platform independent

Open two console windows in the project directory.

In the first one type:

```
node resources/server/server.js
```

In the second one type:

```
gulp
```

### Windows

```
s\dev
```
